#https://petrimaki.com/2013/04/28/reading-arduino-serial-ports-in-windows-7/
#https://matplotlib.org/tutorials/introductory/usage.html#sphx-glr-tutorials-introductory-usage-py
#https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.loadtxt.html
#http://www.toptechboy.com/tutorial/python-with-arduino-lesson-11-plotting-and-graphing-live-data-from-arduino-with-matplotlib/

import serial # import Serial Library
#import numpy  # Import numpy
import matplotlib.pyplot as plt #import matplotlib library
from drawnow import *
 
tim=[]
TJ=[]
TT=[]
arduinoData = serial.Serial('COM5', 115200) #Creating our serial object named arduinoData
plt.ion() #Tell matplotlib you want interactive mode to plot live data
cnt=0
 
def makeFig(): #Create a function that makes our desired plot
    plt.plot(tim, TJ, label='Thermocouple J')
    plt.plot(tim, TT, label='Thermocouple T')

    plt.xlabel('Time')
    plt.ylabel('Temperature')
    
    plt.legend()
    #plt.ylim(80,90)                                 #Set y min and max values
    #plt.title('My Live Streaming Sensor Data')      #Plot the title
    plt.grid(True)                                  #Turn the grid on
    #plt.ylabel('Temp F')                            #Set ylabels
    #plt.plot(tempF, 'ro-', label='Degrees F')       #plot the temperature
    #plt.legend(loc='upper left')                    #plot the legend
    #plt2=plt.twinx()                                #Create a second y axis
    #plt.ylim(93450,93525)                           #Set limits of second y axis- adjust to readings you are getting
    #plt2.plot(pressure, 'b^-', label='Pressure (Pa)') #plot pressure data
    #plt2.set_ylabel('Pressrue (Pa)')                    #label second y axis
    #plt2.ticklabel_format(useOffset=False)           #Force matplotlib to NOT autoscale y axis
    #plt2.legend(loc='upper right')                  #plot the legend
    
 
while True: # While loop that loops forever
    while (arduinoData.inWaiting()==0): #Wait here until there is data
        pass #do nothing
    arduinoString = arduinoData.readline() #read the line of text from the serial port
    if arduinoString != '':
        try:
            dataArray = arduinoString.split('   ')   #Split it into an array called dataArray
            time = float(dataArray[0])            #Convert first element to floating number and put in temp
            ThermoJ = float(dataArray[1])  
            ThermoT = float(dataArray[2])           #Convert second element to floating number and put in P
            tim.append(time)                     #Build our tempF array by appending temp readings
            TJ.append(ThermoJ)    
            TT.append(ThermoT)                 #Building our pressure array by appending P readings
            drawnow(makeFig)                       #Call drawnow to update our live graph
            plt.pause(.000001)                     #Pause Briefly. Important to keep drawnow from crashing
            cnt=cnt+1
            if(cnt>50):                            #If you have 50 or more points, delete the first one from the array
                tim.pop(0)                       #This allows us to just see the last 50 data points
                TJ.pop(0)
                TT.pop(0)
        except ValueError:
            arduinoString = arduinoString
        except IndexError:
            arduinoString = arduinoString