//https://learn.adafruit.com/adafruit-max31856-thermocouple-amplifier

#include <Adafruit_MAX31856.h>

//Use software SPI: CS, DI, DO, CLK
//Thermocouple type J
Adafruit_MAX31856 maxthermoJ = Adafruit_MAX31856(10, 11, 12, 13);
//Thermocouple type T
Adafruit_MAX31856 maxthermoT = Adafruit_MAX31856(9, 11, 12, 13);
//use hardware SPI, just pass in the CS pin
//Adafruit_MAX31856 maxthermo = Adafruit_MAX31856(10);

//time
double time;

void setup() 
{
  Serial.begin(115200);
  //Serial.println("MAX31856 thermocouple test");

  maxthermoJ.begin();
  maxthermoT.begin();

  maxthermoJ.setThermocoupleType(MAX31856_TCTYPE_J);
  maxthermoT.setThermocoupleType(MAX31856_TCTYPE_T);

  //Serial.print("Thermocouple J type: ");
  //switch (maxthermoJ.getThermocoupleType() ) {
    //case MAX31856_TCTYPE_B: Serial.println("B Type"); break;
    //case MAX31856_TCTYPE_E: Serial.println("E Type"); break;
    //case MAX31856_TCTYPE_J: Serial.println("J Type"); break;
    //case MAX31856_TCTYPE_K: Serial.println("K Type"); break;
    //case MAX31856_TCTYPE_N: Serial.println("N Type"); break;
    //case MAX31856_TCTYPE_R: Serial.println("R Type"); break;
    //case MAX31856_TCTYPE_S: Serial.println("S Type"); break;
    //case MAX31856_TCTYPE_T: Serial.println("T Type"); break;
    //case MAX31856_VMODE_G8: Serial.println("Voltage x8 Gain mode"); break;
    //case MAX31856_VMODE_G32: Serial.println("Voltage x8 Gain mode"); break;
    //default: Serial.println("Unknown"); break;
  //}

  //Serial.print("Thermocouple T type: ");
  //switch (maxthermoT.getThermocoupleType() ) {
    //case MAX31856_TCTYPE_B: Serial.println("B Type"); break;
    //case MAX31856_TCTYPE_E: Serial.println("E Type"); break;
    //case MAX31856_TCTYPE_J: Serial.println("J Type"); break;
    //case MAX31856_TCTYPE_K: Serial.println("K Type"); break;
    //case MAX31856_TCTYPE_N: Serial.println("N Type"); break;
    //case MAX31856_TCTYPE_R: Serial.println("R Type"); break;
    //case MAX31856_TCTYPE_S: Serial.println("S Type"); break;
    //case MAX31856_TCTYPE_T: Serial.println("T Type"); break;
    //case MAX31856_VMODE_G8: Serial.println("Voltage x8 Gain mode"); break;
    //case MAX31856_VMODE_G32: Serial.println("Voltage x8 Gain mode"); break;
    //default: Serial.println("Unknown"); break;
  //}
  
  //time
  Serial.begin(9600);
}

void loop() 
{
  //Serial.print("Time: ");
  time = millis();
  Serial.print(time / 1000.0, 2);    
  Serial.print("   ");
  
  //Serial.print("Cold Junction Temp J: "); 
  //Serial.println(maxthermoJ.readCJTemperature());
  
  //Serial.print("Thermocouple Temp J: "); 
  Serial.print(maxthermoJ.readThermocoupleTemperature());

  Serial.print("   "); 
  
 //Serial.print("Cold Junction Temp T: "); 
 //Serial.println(maxthermoT.readCJTemperature());

  //Serial.print("Thermocouple Temp T: "); 
  Serial.println(maxthermoT.readThermocoupleTemperature());
  
  //Check and print any faults
  uint8_t faultJ = maxthermoJ.readFault();
  if (faultJ) 
  {
    if (faultJ & MAX31856_FAULT_CJRANGE) Serial.println("Cold Junction Range Fault");
    if (faultJ & MAX31856_FAULT_TCRANGE) Serial.println("Thermocouple Range Fault");
    if (faultJ & MAX31856_FAULT_CJHIGH)  Serial.println("Cold Junction High Fault");
    if (faultJ & MAX31856_FAULT_CJLOW)   Serial.println("Cold Junction Low Fault");
    if (faultJ & MAX31856_FAULT_TCHIGH)  Serial.println("Thermocouple High Fault");
    if (faultJ & MAX31856_FAULT_TCLOW)   Serial.println("Thermocouple Low Fault");
    if (faultJ & MAX31856_FAULT_OVUV)    Serial.println("Over/Under Voltage Fault");
    if (faultJ & MAX31856_FAULT_OPEN)    Serial.println("Thermocouple Open Fault");
  }

  //Check and print any faults
  uint8_t faultT = maxthermoT.readFault();
  if (faultT) 
  {
    if (faultT & MAX31856_FAULT_CJRANGE) Serial.println("Cold Junction Range Fault");
    if (faultT & MAX31856_FAULT_TCRANGE) Serial.println("Thermocouple Range Fault");
    if (faultT & MAX31856_FAULT_CJHIGH)  Serial.println("Cold Junction High Fault");
    if (faultT & MAX31856_FAULT_CJLOW)   Serial.println("Cold Junction Low Fault");
    if (faultT & MAX31856_FAULT_TCHIGH)  Serial.println("Thermocouple High Fault");
    if (faultT & MAX31856_FAULT_TCLOW)   Serial.println("Thermocouple Low Fault");
    if (faultT & MAX31856_FAULT_OVUV)    Serial.println("Over/Under Voltage Fault");
    if (faultT & MAX31856_FAULT_OPEN)    Serial.println("Thermocouple Open Fault");
  }
  
  //delay(100);
}